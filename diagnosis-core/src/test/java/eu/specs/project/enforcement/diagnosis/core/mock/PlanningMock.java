package eu.specs.project.enforcement.diagnosis.core.mock;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.specs.datamodel.common.ResourceCollection;
import eu.specs.datamodel.enforcement.PlanningActivity;
import eu.specs.project.enforcement.diagnosis.core.util.AppConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@Component
public class PlanningMock {
    private static String SLA_ID = "56BB164F001A8A2CA67C7AAF";

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AppConfig appConfig;

    public void init() throws IOException {
        PlanningActivity planningActivity = objectMapper.readValue(
                this.getClass().getResourceAsStream("/planning-activity.json"), PlanningActivity.class);

        ResourceCollection collection = new ResourceCollection();
        collection.setTotal(1);
        String planningActUrl = appConfig.getPlanningApiAddress() + "/plan-activities/" + planningActivity.getId();
        collection.addItem(new ResourceCollection.Item(SLA_ID, planningActUrl));

        stubFor(get(urlPathEqualTo("/planning-api/plan-activities")).withQueryParam("slaId", equalTo(SLA_ID))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(objectMapper.writeValueAsString(collection))));

        stubFor(get(urlPathEqualTo("/planning-api/plan-activities/" + planningActivity.getId()))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(objectMapper.writeValueAsString(planningActivity))));
    }
}
