package eu.specs.project.enforcement.diagnosis.core.mock;

import org.springframework.stereotype.Component;

import java.io.IOException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@Component
public class SLAManagerMock {

    public void init() throws IOException {

        stubFor(get(urlPathEqualTo("/sla-manager/cloud-sla/slas/56BB164F001A8A2CA67C7AAF/status"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "text/plain")
                        .withBody("OBSERVED")));

        stubFor(put(urlPathEqualTo("/sla-manager/cloud-sla/slas/56BB164F001A8A2CA67C7AAF/status"))
                .withHeader("Content-Type", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)));

        stubFor(post(urlPathMatching("/sla-manager/cloud-sla/slas/[\\w-]+/(signalAlert|reNegotiate)"))
                .willReturn(aResponse()
                        .withStatus(200)));

        stubFor(post(urlPathMatching("/sla-manager/cloud-sla/slas/[\\w-]+/annotations"))
                .willReturn(aResponse()
                        .withStatus(200)));
    }

}
