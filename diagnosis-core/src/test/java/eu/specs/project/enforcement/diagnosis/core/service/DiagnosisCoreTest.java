package eu.specs.project.enforcement.diagnosis.core.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import eu.specs.datamodel.enforcement.DiagnosisActivity;
import eu.specs.datamodel.enforcement.Notification;
import eu.specs.project.enforcement.diagnosis.core.mock.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.SocketUtils;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:diagnosis-core-context-test.xml")
public class DiagnosisCoreTest {
    private static final Logger logger = LogManager.getLogger(DiagnosisCoreTest.class);

    @Autowired
    private DiagnosisActivityService daService;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private EventArchiverMock eventArchiverMock;

    @Autowired
    private PlanningMock planningMock;

    @Autowired
    private ImplementationMock implementationMock;

    @Autowired
    private SLAManagerMock slaManagerMock;

    @Autowired
    private RdsMock rdsMock;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(Integer.parseInt(System.getProperty("wiremock.port")));

    @BeforeClass
    public static void setSystemProps() {
        int port = SocketUtils.findAvailableTcpPort();
        logger.debug("Using port {} for WireMock server.", port);
        System.setProperty("wiremock.port", Integer.toString(port));
    }

    @Before
    public void setUp() throws Exception {
        eventArchiverMock.init();
        planningMock.init();
        implementationMock.init();
        slaManagerMock.init();
        rdsMock.init();
    }

    @Test
    public void testDiagnosisCore() throws Exception {
        Notification notification = objectMapper.readValue(
                this.getClass().getResourceAsStream("/notification.json"), Notification.class);
        String slaId = notification.getSlaId();

        DiagnosisActivity diagnosisActivity = daService.create(notification);
        assertNotNull(diagnosisActivity.getId());
        assertEquals(diagnosisActivity.getState(), DiagnosisActivity.Status.RECEIVED);

        Date startTime = new Date();
        while (diagnosisActivity.getState() != DiagnosisActivity.Status.SOLVED &&
                diagnosisActivity.getState() != DiagnosisActivity.Status.ERROR) {
            Thread.sleep(1000);
            if (new Date().getTime() - startTime.getTime() > 15000) {
                fail("Timeout waiting for the diagnosis activity.");
            }
        }

        assertEquals(diagnosisActivity.getState(), DiagnosisActivity.Status.SOLVED);
        assertEquals(diagnosisActivity.getEventImpact(), 3);
        assertEquals(diagnosisActivity.getClassification(), DiagnosisActivity.Classification.ALERT);
        assertEquals(diagnosisActivity.getSlaId(), slaId);
        assertEquals(diagnosisActivity.getNotificationId(), notification.getId());

        DiagnosisActivity diagnosisActivity1 = daService.findById(diagnosisActivity.getId());
        assertEquals(diagnosisActivity1.getId(), diagnosisActivity.getId());
        DiagnosisActivityService.DiagActFilter diagActFilter = new DiagnosisActivityService.DiagActFilter();
        diagActFilter.setSlaId(slaId);
        diagActFilter.setOffset(0);
        diagActFilter.setLimit(1);
        List<DiagnosisActivity> diagnosisActivities = daService.findAll(diagActFilter);
        assertEquals(diagnosisActivities.size(), 1);
        assertEquals(diagnosisActivities.get(0).getId(), diagnosisActivity.getId());

        Notification notification1 = notificationService.findById(diagnosisActivity.getNotificationId());
        assertEquals(notification1.getMeasurementId(), notification.getMeasurementId());
        NotificationService.NotificationsFilter notificationsFilter = new NotificationService.NotificationsFilter();
        notificationsFilter.setSlaId(slaId);
        notificationsFilter.setComponent(notification.getComponent());
        notificationsFilter.setObject(notification.getObject());
        notificationsFilter.setOffset(0);
        notificationsFilter.setLimit(1);
        List<Notification> notifications = notificationService.findAll(notificationsFilter);
        assertEquals(notifications.size(), 1);
        assertEquals(notifications.get(0).getId(), notification.getId());
    }

    @Test
    public void testInvalidNotification() throws Exception {
        Notification notification = objectMapper.readValue(
                this.getClass().getResourceAsStream("/notification.json"), Notification.class);
        notification.setMeasurementId("INVALID");

        DiagnosisActivity diagnosisActivity = daService.create(notification);

        Date startTime = new Date();
        while (diagnosisActivity.getState() != DiagnosisActivity.Status.SOLVED &&
                diagnosisActivity.getState() != DiagnosisActivity.Status.ERROR) {
            Thread.sleep(1000);
            if (new Date().getTime() - startTime.getTime() > 10000) {
                fail("Timeout waiting for the diagnosis activity.");
            }
        }
        assertEquals(diagnosisActivity.getState(), DiagnosisActivity.Status.ERROR);
        assertNotNull(diagnosisActivity.getAnnotations().get(0).getName(), "error");
    }
}