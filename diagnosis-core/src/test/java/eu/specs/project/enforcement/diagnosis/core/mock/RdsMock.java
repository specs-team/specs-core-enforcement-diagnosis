package eu.specs.project.enforcement.diagnosis.core.mock;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.specs.datamodel.enforcement.RemActivity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.UUID;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@Component
public class RdsMock {

    @Autowired
    private ObjectMapper objectMapper;

    public void init() throws IOException {

        RemActivity remActivity = new RemActivity();
        remActivity.setId(UUID.randomUUID().toString());

        stubFor(post(urlPathEqualTo("/rds-api/rem-activities"))
                        .withHeader("Content-Type", equalTo("application/json"))
                        .willReturn(aResponse()
                                .withStatus(200)
                                .withHeader("Content-Type", "application/json")
                                .withBody(objectMapper.writeValueAsString(remActivity)))
        );

        stubFor(get(urlPathMatching("/rds-api/rem-activities/[\\w-]+/status"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "text/plain")
                        .withBody("SOLVED")));
    }
}
