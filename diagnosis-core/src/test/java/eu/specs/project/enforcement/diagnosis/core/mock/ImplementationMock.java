package eu.specs.project.enforcement.diagnosis.core.mock;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.specs.datamodel.enforcement.ImplementationPlan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@Component
public class ImplementationMock {

    @Autowired
    private ObjectMapper objectMapper;

    public void init() throws IOException {
        ImplementationPlan implPlan = objectMapper.readValue(
                this.getClass().getResourceAsStream("/impl-plan.json"), ImplementationPlan.class);

        stubFor(get(urlPathEqualTo("/implementation-api/impl-plans/" + implPlan.getId()))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(objectMapper.writeValueAsString(implPlan))));
    }
}
