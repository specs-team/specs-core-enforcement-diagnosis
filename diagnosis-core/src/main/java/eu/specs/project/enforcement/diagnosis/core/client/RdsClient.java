package eu.specs.project.enforcement.diagnosis.core.client;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.specs.datamodel.enforcement.ImplementationPlan;
import eu.specs.datamodel.enforcement.RemActivity;
import eu.specs.project.enforcement.diagnosis.core.exception.DiagnosisException;
import eu.specs.project.enforcement.diagnosis.core.util.AppConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

@Component
public class RdsClient {
    private static String REM_ACTIVITIES_PATH = "/rem-activities";
    private static String REM_ACTIVITY_PATH = "/rem-activities/{id}";
    private static final Logger logger = LogManager.getLogger(RdsClient.class);

    private Client client;
    private WebTarget rdsTarget;

    @Autowired
    public RdsClient(AppConfig appConfig) {
        client = ClientBuilder.newBuilder()
                .register(JacksonJsonProvider.class)
                .build();

        rdsTarget = client.target(appConfig.getRdsApiAddress());
    }

    public RemActivity createRemActivity(RemActivity remActivityData) throws DiagnosisException {
        try {
            logger.debug("Starting remediation at RDS...");
            RemActivity remActivity = rdsTarget
                    .path(REM_ACTIVITIES_PATH)
                    .request(MediaType.APPLICATION_JSON)
                    .post(Entity.json(remActivityData), RemActivity.class);

            logger.debug("Remediation activity created successfully: {}", remActivity.getId());
            return remActivity;

        } catch (Exception e) {
            throw new DiagnosisException("Failed to create remediation activity: " + e.getMessage(), e);
        }
    }

    public RemActivity retrieveRemActivity(String remActId) throws DiagnosisException {
        try {
            logger.debug("Retrieving remediation activity {}...", remActId);
            RemActivity remActivity = rdsTarget
                    .path(REM_ACTIVITY_PATH)
                    .resolveTemplate("id", remActId)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .get(RemActivity.class);

            logger.debug("Remediation activity retrieved successfully.");
            return remActivity;
        } catch (Exception e) {
            throw new DiagnosisException("Failed to retrieve remediation activity: " + e.getMessage(), e);
        }
    }

    public RemActivity.Status retrieveRemActivityStatus(String remActivityId) throws DiagnosisException {
        try {
            String statusString = rdsTarget
                    .path(REM_ACTIVITY_PATH + "/status")
                    .resolveTemplate("id", remActivityId)
                    .request()
                    .get(String.class);

            RemActivity.Status status = RemActivity.Status.valueOf(statusString);
            return status;

        } catch (Exception e) {
            throw new DiagnosisException("Failed to retrieve remediation activity status: " + e.getMessage(), e);
        }
    }
}
