package eu.specs.project.enforcement.diagnosis.core.repository;

import eu.specs.datamodel.enforcement.DiagnosisActivity;
import eu.specs.project.enforcement.diagnosis.core.service.DiagnosisActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DiagnosisActivityRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    public DiagnosisActivity findById(String daId) {
        return mongoTemplate.findById(daId, DiagnosisActivity.class);
    }

    public DiagnosisActivity findByNotificationId(String notificationId) {
        Query query = new Query().addCriteria(Criteria.where("notificationId").is(notificationId));
        return mongoTemplate.findOne(query, DiagnosisActivity.class);
    }

    public List<DiagnosisActivity> findAll(DiagnosisActivityService.DiagActFilter filter) {
        Query query = new Query();
        if (filter.getSlaId() != null) {
            query.addCriteria(Criteria.where("slaId").is(filter.getSlaId()));
        }
        if (filter.getLimit() != null) {
            query.limit(filter.getLimit());
        }
        if (filter.getOffset() != null) {
            query.skip(filter.getOffset());
        }

        query.fields().include("id");
        return mongoTemplate.find(query, DiagnosisActivity.class);
    }

    public void save(DiagnosisActivity diagnosisactivity) {
        mongoTemplate.save(diagnosisactivity);
    }

    public void delete(DiagnosisActivity diagnosisActivity) {
        mongoTemplate.remove(diagnosisActivity);
    }

    public DiagnosisActivity.Status getStatus(String diagActId) {
        Query query = new Query().addCriteria(Criteria.where("notificationId").is(diagActId));
        query.fields().include("state");
        DiagnosisActivity diagnosisActivity = mongoTemplate.findOne(query, DiagnosisActivity.class);
        return diagnosisActivity.getState();
    }
}
