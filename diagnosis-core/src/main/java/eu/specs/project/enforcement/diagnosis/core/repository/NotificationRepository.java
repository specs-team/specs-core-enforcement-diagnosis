package eu.specs.project.enforcement.diagnosis.core.repository;

import eu.specs.datamodel.enforcement.Notification;
import eu.specs.project.enforcement.diagnosis.core.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class NotificationRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    public Notification findById(String notificationId) {
        return mongoTemplate.findById(notificationId, Notification.class);
    }

    public List<Notification> findAll(NotificationService.NotificationsFilter filter) {
        Query query = new Query();
        if (filter.getSlaId() != null) {
            query.addCriteria(Criteria.where("slaId").is(filter.getSlaId()));
        }
        if (filter.getComponent() != null) {
            query.addCriteria(Criteria.where("component").is(filter.getComponent()));
        }
        if (filter.getObject() != null) {
            query.addCriteria(Criteria.where("object").is(filter.getObject()));
        }
        if (filter.getLimit() != null) {
            query.limit(filter.getLimit());
        }
        if (filter.getOffset() != null) {
            query.skip(filter.getOffset());
        }

        query.fields().include("id");
        return mongoTemplate.find(query, Notification.class);
    }

    public void save(Notification notification) {
        mongoTemplate.save(notification);
    }

    public void update(Notification notification) {
        mongoTemplate.save(notification);
    }

    public void delete(Notification notification) {
        mongoTemplate.remove(notification);
    }
}
