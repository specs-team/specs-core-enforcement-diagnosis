package eu.specs.project.enforcement.diagnosis.core.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AppConfig {

    @Value("${sla-manager-api.address}")
    private String slaManagerApiAddress;

    @Value("${planning-api.address}")
    private String planningApiAddress;

    @Value("${implementation-api.address}")
    private String implementationApiAddress;

    @Value("${rds-api.address}")
    private String rdsApiAddress;

    @Value("${auditing-api.address}")
    private String auditingApiAddress;

    @Value("${event-archiver.address}")
    private String eventArchiverAddress;

    @Value("${remediation-activity.timeout}")
    private int remActivityTimeout;

    public String getSlaManagerApiAddress() {
        return slaManagerApiAddress;
    }

    public String getPlanningApiAddress() {
        return planningApiAddress;
    }

    public String getImplementationApiAddress() {
        return implementationApiAddress;
    }

    public String getRdsApiAddress() {
        return rdsApiAddress;
    }

    public String getAuditingApiAddress() {
        return auditingApiAddress;
    }

    public String getEventArchiverAddress() {
        return eventArchiverAddress;
    }

    public int getRemActivityTimeout() {
        return remActivityTimeout;
    }
}
