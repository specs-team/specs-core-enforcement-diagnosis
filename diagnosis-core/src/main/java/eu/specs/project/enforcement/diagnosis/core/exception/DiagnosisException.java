package eu.specs.project.enforcement.diagnosis.core.exception;

public class DiagnosisException extends Exception {

    public DiagnosisException() {
        super();
    }

    public DiagnosisException(String message) {
        super(message);
    }

    public DiagnosisException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
