package eu.specs.project.enforcement.diagnosis.core.client;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.specs.datamodel.common.ResourceCollection;
import eu.specs.datamodel.enforcement.PlanningActivity;
import eu.specs.project.enforcement.diagnosis.core.exception.DiagnosisException;
import eu.specs.project.enforcement.diagnosis.core.util.AppConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

@Component
public class PlanningClient {
    private static String PLANNING_ACTIVITIES_PATH = "/plan-activities";
    private static final Logger logger = LogManager.getLogger(PlanningClient.class);

    private Client client;
    private WebTarget planningTarget;

    @Autowired
    public PlanningClient(AppConfig appConfig) {
        client = ClientBuilder.newBuilder()
                .register(JacksonJsonProvider.class)
                .build();

        planningTarget = client.target(appConfig.getPlanningApiAddress());
    }

    public PlanningActivity retrievePlanningActivity(String slaId) throws DiagnosisException {
        try {
            logger.debug("Retrieving planning activity for the SLA {}...", slaId);
            ResourceCollection collection = planningTarget
                    .path(PLANNING_ACTIVITIES_PATH)
                    .queryParam("slaId", slaId)
                    .request()
                    .get(ResourceCollection.class);

            if (collection.getTotal() != 1) {
                throw new DiagnosisException(String.format("No planning activity found for the SLA %s.", slaId));
            }

            PlanningActivity planningActivity = client
                    .target(collection.getItemList().get(0).getItem())
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .get(PlanningActivity.class);

            logger.debug("Planning activity retrieved successfully.");
            return planningActivity;

        } catch (Exception e) {
            throw new DiagnosisException("Failed to retrieve planning activity: " + e.getMessage(), e);
        }
    }
}
