package eu.specs.project.enforcement.diagnosis.core.service;

import eu.specs.datamodel.common.SlaState;
import eu.specs.datamodel.enforcement.DiagnosisActivity;
import eu.specs.datamodel.enforcement.Notification;
import eu.specs.project.enforcement.diagnosis.core.client.SlaManagerClient;
import eu.specs.project.enforcement.diagnosis.core.exception.DiagnosisException;
import eu.specs.project.enforcement.diagnosis.core.processor.DiagnosisActivityProcessor;
import eu.specs.project.enforcement.diagnosis.core.repository.DiagnosisActivityRepository;
import eu.specs.project.enforcement.diagnosis.core.repository.NotificationRepository;
import eu.specs.project.enforcement.diagnosis.core.util.JsonDumper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class DiagnosisActivityService {
    private static final Logger logger = LogManager.getLogger(DiagnosisActivityService.class);
    public static final int EXECUTOR_NUMBER_OF_THREADS = 3;
    public static final Set<SlaState> REQUIRED_SLA_STATES = new HashSet<>(Arrays.asList(
            SlaState.OBSERVED));
    private ExecutorService executorService;

    @Autowired
    private NotificationRepository notificationRepository;

    @Autowired
    private DiagnosisActivityRepository diagActRepository;

    @Autowired
    private DiagnosisActivityProcessor diagActProcessor;

    @Autowired
    private SlaManagerClient slaManagerClient;

    public DiagnosisActivityService() {
        executorService = Executors.newFixedThreadPool(EXECUTOR_NUMBER_OF_THREADS);
    }

    public DiagnosisActivity create(Notification notification) throws DiagnosisException {
        if (logger.isTraceEnabled()) {
            logger.trace("Notification received:\n{}", JsonDumper.dump(notification));
        }
        notification.setId(UUID.randomUUID().toString());
        notificationRepository.save(notification);

        // check if SLA is in correct state
        SlaState slaState = slaManagerClient.retrieveSlaState(notification.getSlaId());
        if (!REQUIRED_SLA_STATES.contains(slaState)) {
            logger.debug("Notification {} for the SLA {} discarded because SLA is not in the correct state. " +
                    "Current state is {} but should be one of {}.",
                    notification.getId(), notification.getSlaId(), slaState, REQUIRED_SLA_STATES);
            return null;
        }

        logger.trace("Creating diagnosis activity...");
        final DiagnosisActivity diagnosisActivity = new DiagnosisActivity();
        diagnosisActivity.setId(UUID.randomUUID().toString());
        diagnosisActivity.setCreationTime(new Date());
        diagnosisActivity.setState(DiagnosisActivity.Status.RECEIVED);
        diagnosisActivity.setNotificationId(notification.getId());
        diagnosisActivity.setSlaId(notification.getSlaId());
        diagnosisActivity.setComponent(notification.getComponent());
        diagnosisActivity.setObject(notification.getObject());
        diagActRepository.save(diagnosisActivity);

        if (logger.isTraceEnabled()) {
            logger.debug("Diagnosis activity has been created successfully:\n{}", JsonDumper.dump(diagnosisActivity));
        }

        executorService.execute(new Runnable() {
            @Override
            public void run() {
                diagActProcessor.classify(diagnosisActivity);
            }
        });

        return diagnosisActivity;
    }

    public List<DiagnosisActivity> findAll(DiagActFilter filter) {
        return diagActRepository.findAll(filter);
    }

    public DiagnosisActivity findById(String diagActId) {
        return diagActRepository.findById(diagActId);
    }

    public DiagnosisActivity.Status getStatus(String diagActId) {
        return diagActRepository.getStatus(diagActId);
    }

    public static class DiagActFilter {
        private String slaId;
        private Integer offset;
        private Integer limit;

        public String getSlaId() {
            return slaId;
        }

        public void setSlaId(String slaId) {
            this.slaId = slaId;
        }

        public Integer getOffset() {
            return offset;
        }

        public void setOffset(Integer offset) {
            this.offset = offset;
        }

        public Integer getLimit() {
            return limit;
        }

        public void setLimit(Integer limit) {
            this.limit = limit;
        }
    }
}
