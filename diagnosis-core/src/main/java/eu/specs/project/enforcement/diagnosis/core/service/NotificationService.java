package eu.specs.project.enforcement.diagnosis.core.service;

import eu.specs.datamodel.enforcement.Notification;
import eu.specs.project.enforcement.diagnosis.core.repository.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotificationService {

    @Autowired
    private NotificationRepository notificationRepository;

    public List<Notification> findAll(NotificationsFilter filter) {
        return notificationRepository.findAll(filter);
    }

    public Notification findById(String notificationId) {
        return notificationRepository.findById(notificationId);
    }

    public static class NotificationsFilter {
        private String slaId;
        private String component;
        private String object;
        private Integer offset;
        private Integer limit;

        public String getSlaId() {
            return slaId;
        }

        public void setSlaId(String slaId) {
            this.slaId = slaId;
        }

        public String getComponent() {
            return component;
        }

        public void setComponent(String component) {
            this.component = component;
        }

        public String getObject() {
            return object;
        }

        public void setObject(String object) {
            this.object = object;
        }

        public Integer getOffset() {
            return offset;
        }

        public void setOffset(Integer offset) {
            this.offset = offset;
        }

        public Integer getLimit() {
            return limit;
        }

        public void setLimit(Integer limit) {
            this.limit = limit;
        }
    }
}
