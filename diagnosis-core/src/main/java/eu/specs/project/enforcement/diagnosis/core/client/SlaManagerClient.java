package eu.specs.project.enforcement.diagnosis.core.client;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.common.SlaState;
import eu.specs.project.enforcement.diagnosis.core.exception.DiagnosisException;
import eu.specs.project.enforcement.diagnosis.core.util.AppConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
public class SlaManagerClient {
    private static String SLA_PATH = "/slas/{id}";
    private static String SLA_STATE_PATH = "/slas/{id}/status";
    private static String ANNOTATIONS_PATH = "/slas/{id}/annotations";
    private static final Logger logger = LogManager.getLogger(SlaManagerClient.class);

    private WebTarget slaManagerTarget;
    private Map<SlaState, String> slaStateCommands;

    @Autowired
    public SlaManagerClient(AppConfig appConfig) {
        Client client = ClientBuilder.newBuilder()
                .register(JacksonJsonProvider.class)
                .build();

        slaManagerTarget = client.target(appConfig.getSlaManagerApiAddress());

        slaStateCommands = new HashMap<>();
        slaStateCommands.put(SlaState.ALERTED, "signalAlert");
        slaStateCommands.put(SlaState.VIOLATED, "signalViolation");
    }

    public SlaState retrieveSlaState(String slaId) throws DiagnosisException {
        try {
            logger.debug("Retrieving SLA {} state...", slaId);
            String slaStateString = slaManagerTarget
                    .path(SLA_STATE_PATH)
                    .resolveTemplate("id", slaId)
                    .request(MediaType.TEXT_PLAIN)
                    .get(String.class);
            SlaState slaState = SlaState.valueOf(slaStateString);
            logger.debug("SLA state has been retrieved successfully.");
            return slaState;

        } catch (Exception e) {
            throw new DiagnosisException(String.format("Failed to retrieve SLA %s state: %s",
                    slaId, e.getMessage()), e);
        }
    }

    public void setSlaState(String slaId, SlaState slaState) throws DiagnosisException {
        logger.debug("Setting SLA {} state to {}...", slaId, slaState);
        String command = slaStateCommands.get(slaState);
        if (command == null) {
            throw new UnsupportedOperationException("Invalid SLA state: " + slaState);
        }

        try {
            Response response = slaManagerTarget
                    .path(SLA_PATH + "/" + slaStateCommands.get(slaState))
                    .resolveTemplate("id", slaId)
                    .request()
                    .post(Entity.json(null));

            if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
                String content = response.readEntity(String.class);
                logger.error("Failed to set SLA state: {}\n{}", response.getStatusInfo(), content);
                throw new DiagnosisException(String.format("Failed to set SLA state to %s.", slaState));
            } else {
                logger.debug("SLA state set successfully.");
            }
        } catch (Exception e) {
            throw new DiagnosisException(String.format("Failed to set SLA state: %s",
                    e.getMessage()), e);
        }
    }

    public void addAnnotation(String slaId, String description) throws DiagnosisException {
        logger.debug("Adding annotation to the SLA {}...", slaId);

        try {
            Form form = new Form();
            form.param("id", UUID.randomUUID().toString());
            form.param("description", description);
            form.param("timestamp", Long.toString(new Date().getTime()));

            Response response = slaManagerTarget
                    .path(ANNOTATIONS_PATH)
                    .resolveTemplate("id", slaId)
                    .request()
                    .post(Entity.form(form));

            if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
                throw new DiagnosisException("Invalid HTTP response from SLA Manager: " + response.getStatusInfo());
            } else {
                logger.debug("Annotation created successfully.");
            }
        } catch (Exception e) {
            throw new DiagnosisException(String.format("Failed to create annotation for the SLA %s: %s",
                    slaId, e.getMessage()), e);
        }
    }
}
