package eu.specs.project.enforcement.diagnosis.core.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class JsonDumper {
    private static final Logger logger = LogManager.getLogger(JsonDumper.class);
    private static ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
    }

    private JsonDumper() {
    }

    public static String dump(Object value) {
        try {
            return mapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            logger.warn("JsonDumper failed: " + e.getMessage(), e);
            return "Failed to dump object.";
        }
    }
}
