package eu.specs.project.enforcement.diagnosis.core.processor;

import eu.specs.datamodel.common.Annotation;
import eu.specs.datamodel.common.SlaState;
import eu.specs.datamodel.enforcement.*;
import eu.specs.datamodel.enforcement.DiagnosisActivity.Classification;
import eu.specs.datamodel.enforcement.Measurement.MonitoringEvent.EventType;
import eu.specs.project.enforcement.diagnosis.core.client.ImplementationClient;
import eu.specs.project.enforcement.diagnosis.core.client.PlanningClient;
import eu.specs.project.enforcement.diagnosis.core.client.RdsClient;
import eu.specs.project.enforcement.diagnosis.core.client.SlaManagerClient;
import eu.specs.project.enforcement.diagnosis.core.exception.DiagnosisException;
import eu.specs.project.enforcement.diagnosis.core.repository.DiagnosisActivityRepository;
import eu.specs.project.enforcement.diagnosis.core.repository.NotificationRepository;
import eu.specs.project.enforcement.diagnosis.core.util.AppConfig;
import eu.specs.project.enforcement.diagnosis.core.util.JsonDumper;
import eu.specsproject.core.slaplatform.auditing.client.AuditClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.PriorityBlockingQueue;

@Component
public class DiagnosisActivityProcessor {
    private static final Logger logger = LogManager.getLogger(DiagnosisActivityProcessor.class);
    private static final int QUEUE_CAPACITY = 5000;

    private static Map<RiskLevelTableKey, Integer> riskLevelTable = createRiskLevelTable();
    private RemediationEngine remediationEngine = new RemediationEngine();

    @Autowired
    private DiagnosisActivityRepository diagActRepository;

    @Autowired
    private NotificationRepository notificationRepository;

    @Autowired
    private PlanningClient planningClient;

    @Autowired
    private ImplementationClient implClient;

    @Autowired
    private SlaManagerClient slaManagerClient;

    @Autowired
    private AuditClient auditClient;

    @Autowired
    private RdsClient rdsClient;

    @Autowired
    private AppConfig appConfig;

    public DiagnosisActivityProcessor() {
        Thread remediationEngineThread = new Thread(remediationEngine);
        remediationEngineThread.start();
    }

    public void classify(DiagnosisActivity diagnosisActivity) {
        logger.trace("Processing diagnosis activity {}...", diagnosisActivity.getId());
        try {

            classifyImpl(diagnosisActivity);
            logger.debug("Diagnosis activity {} has been classified successfully.", diagnosisActivity.getId());

            if (logger.isTraceEnabled()) {
                logger.trace("Diagnosis activity:\n{}", JsonDumper.dump(diagnosisActivity));
            }

        } catch (Exception e) {
            logger.error("Diagnosis activity failed: " + e.getMessage(), e);
            diagnosisActivity.setState(DiagnosisActivity.Status.ERROR);
            diagnosisActivity.addAnnotation(
                    new Annotation("error", "Diagnosis activity failed: " + e.getMessage(), new Date()));
            diagActRepository.save(diagnosisActivity);
        }
    }

    private void classifyImpl(DiagnosisActivity diagnosisActivity) throws DiagnosisException {

        String slaId = diagnosisActivity.getSlaId();

        auditClient.logComponentActivity("Diagnosis", CompActivity.ComponentState.ACTIVATED, slaId, SlaState.OBSERVED);

        PlanningActivity planningActivity = planningClient.retrievePlanningActivity(slaId);

        ImplementationPlan implPlan = implClient.retrieveImplPlan(planningActivity.getActivePlanId());
        diagnosisActivity.setPlanId(implPlan.getId());

        Notification notification = notificationRepository.findById(diagnosisActivity.getNotificationId());
        if (logger.isTraceEnabled()) {
            logger.trace("Notification:\n{}", JsonDumper.dump(notification));
        }

        // get corresponding measurement from the implementation plan
        Measurement measurement = getMeasurement(implPlan, notification.getMeasurementId());
        if (measurement == null) {
            throw new DiagnosisException(String.format(
                    "Invalid measurement in the notification: %s. No such measurement in the implementation plan.",
                    notification.getMeasurementId()));
        }

        List<Slo> affectedSlos = new ArrayList<>();
        for (Slo slo : implPlan.getSlos()) {
            if (measurement.getMetrics().contains(slo.getMetricId())) {
                affectedSlos.add(slo);
                diagnosisActivity.addAffectedSlo(slo.getId());
                logger.debug("Affected SLO found: {}", slo.getId());
            }
        }

        if (affectedSlos.isEmpty()) {
            logger.debug("No affected SLOs found.");
            return;
        }

        // metric -> SLO mapping
        Map<String, Slo> metricSloMapping = new HashMap<>();
        for (Slo slo : implPlan.getSlos()) {
            metricSloMapping.put(slo.getMetricId(), slo);
        }

        logger.debug("Checking if monitoring event is in violation...");
        boolean isViolated = evaluateCondition(measurement, notification.getValue(), metricSloMapping);
        logger.debug("Monitoring event: isViolated={}", isViolated);
        if (!isViolated) {
            logger.debug("The monitoring event from measurement {} is not violated.", measurement.getId());
            diagnosisActivity.setClassification(Classification.FALSE_POSITIVE);
            diagActRepository.save(diagnosisActivity);
            return;
        }

        EventType eventType = measurement.getMonitoringEvent().getEventType();
        logger.debug("Event type is {}.", eventType);

        List<Slo.ImportanceLevel> importanceLevels = new ArrayList<>();
        for (Slo slo : affectedSlos) {
            importanceLevels.add(slo.getImportanceLevel());
        }
        Slo.ImportanceLevel maxImportanceLevel = Collections.max(importanceLevels);

        Integer severityLevel = riskLevelTable.get(new RiskLevelTableKey(eventType, maxImportanceLevel));
        if (severityLevel == null) {
            throw new DiagnosisException("Unable to determine severity level of the monitoring event.");
        }

        SlaState slaState;

        if (eventType == EventType.ALERT) {
            diagnosisActivity.setClassification(Classification.ALERT);
            slaState = SlaState.ALERTED;
        } else if (eventType == EventType.VIOLATION) {
            diagnosisActivity.setClassification(Classification.VIOLATION);
            slaState = SlaState.VIOLATED;
        } else {
            throw new DiagnosisException("Unexpected event type: " + eventType);
        }

        diagnosisActivity.setState(DiagnosisActivity.Status.CLASSIFIED);
        diagnosisActivity.addRootCause(measurement.getMonitoringEvent().getDescription());
        diagnosisActivity.setMeasurementId(measurement.getId());
        diagnosisActivity.setMeasurementTime(notification.getTimestamp());
        diagnosisActivity.setEventImpact(severityLevel);
        diagnosisActivity.setEventId(measurement.getMonitoringEvent().getId());
        diagActRepository.save(diagnosisActivity);

        // check current SLA state and set new SLA state if needed
        SlaState slaStateCurrent = slaManagerClient.retrieveSlaState(slaId);
        logger.debug("Current SLA state {}, new SLA state {}.", slaStateCurrent, slaState);
        if (slaState == SlaState.ALERTED && slaStateCurrent == SlaState.OBSERVED) {
            slaManagerClient.setSlaState(slaId, slaState);
        } else if (slaState == SlaState.VIOLATED &&
                (slaStateCurrent == SlaState.OBSERVED || slaStateCurrent == SlaState.ALERTED)) {
            slaManagerClient.setSlaState(slaId, slaState);
        }

        // add SLA annotation
        createSLAViolationAnnotation(notification, diagnosisActivity, affectedSlos, measurement, metricSloMapping);

        // create DiagnosisMonEvent audit event
        DiagnosisMonEvent diagMonEvent = new DiagnosisMonEvent();
        diagMonEvent.setId(diagnosisActivity.getId());
        diagMonEvent.setSlaId(diagnosisActivity.getSlaId());
        diagMonEvent.setCreationTime(new Date());
        diagMonEvent.setEventType(eventType.name());
        diagMonEvent.setMeasurementTime(diagnosisActivity.getMeasurementTime());
        diagMonEvent.setNotificationId(notification.getId());
        diagMonEvent.setPriortyQueueTimestamp(diagnosisActivity.getPriorityQueueTimestamp());
        diagMonEvent.setRootCauses(diagnosisActivity.getRootCauses());
        diagMonEvent.setSlaImpact(diagnosisActivity.getEventImpact());
        diagMonEvent.setSloIds(diagnosisActivity.getAffectedSlos());
        auditClient.logDiagnosisMonEvent(diagMonEvent);

        remediationEngine.add(diagnosisActivity);
    }

    private boolean evaluateCondition(Measurement measurement, String valueString,
                                      Map<String, Slo> metricSloMapping) throws DiagnosisException {

        try {
            Measurement.MonitoringEvent.Condition condition = measurement.getMonitoringEvent().getCondition();
            String thresholdValue;
            if (condition.getThreshold().startsWith("metric:")) {
                String metric = condition.getThreshold().substring("metric:".length());
                thresholdValue = metricSloMapping.get(metric).getValue();
            } else {
                thresholdValue = condition.getThreshold();
            }

            // TODO: how to determine measurement data type? Should be specified in the mechanism?
            DataType dataType;
            if ("true".equals(thresholdValue) || "false".equals(thresholdValue)) {
                dataType = DataType.BOOLEAN;
            } else if (thresholdValue.matches("^-?\\d+(.\\d+)?$")) {
                dataType = DataType.DOUBLE;
            } else if (thresholdValue.matches("^-?\\d+$")) {
                dataType = DataType.INTEGER;
            } else {
                throw new DiagnosisException(String.format("Unable to determine data type of the measurement %s.",
                        measurement.getId()));
            }

            logger.debug("Evaluating {} condition {} {} {}...", dataType, valueString, condition.getOperator(), thresholdValue);

            if (dataType == DataType.INTEGER) {
                int threshold = Integer.parseInt(thresholdValue);
                int value = Integer.parseInt(valueString);
                switch (condition.getOperator()) {
                    case "eq":
                        return value == threshold;
                    case "neq":
                        return value != threshold;
                    case "gt":
                        return value > threshold;
                    case "geq":
                        return value >= threshold;
                    case "lt":
                        return value < threshold;
                    case "leq":
                        return value <= threshold;
                    default:
                        throw new DiagnosisException(String.format(
                                "Invalid operator for data type %s: %s", dataType, condition.getOperator()));
                }
            } else if (dataType == DataType.DOUBLE) {
                double threshold = Double.parseDouble(thresholdValue);
                double value = Double.parseDouble(valueString);
                switch (condition.getOperator()) {
                    case "gt":
                        return value > threshold;
                    case "geq":
                        return value >= threshold;
                    case "lt":
                        return value < threshold;
                    case "leq":
                        return value <= threshold;
                    default:
                        throw new DiagnosisException(String.format(
                                "Invalid operator for data type %s: %s", dataType, condition.getOperator()));
                }
            } else if (dataType == DataType.BOOLEAN) {
                switch (condition.getOperator()) {
                    case "eq":
                        return valueString.equals(thresholdValue);
                    case "neq":
                        return !valueString.equals(thresholdValue);
                    default:
                        throw new DiagnosisException(String.format(
                                "Invalid operator for data type %s: %s", dataType, condition.getOperator()));
                }
            } else {
                throw new DiagnosisException("Invalid data type:" + dataType);
            }
        } catch (Exception e) {
            throw new DiagnosisException(String.format("Failed to evaluate measurement %s condition: %s",
                    measurement.getId(), e.getMessage()), e);
        }
    }

    private Measurement getMeasurement(ImplementationPlan implPlan, String measurementId) {
        for (Measurement measurement : implPlan.getMeasurements()) {
            if (measurement.getId().equals(measurementId)) {
                return measurement;
            }
        }
        return null;
    }

    private RemActivity createRemActivityData(DiagnosisActivity diagnosisActivity) throws DiagnosisException {
        RemActivity remActivity = new RemActivity();
        remActivity.setDiagActivityId(diagnosisActivity.getId());
        remActivity.setComponent(diagnosisActivity.getComponent());
        remActivity.setObject(diagnosisActivity.getObject());
        remActivity.setSlaId(diagnosisActivity.getSlaId());
        remActivity.setPlanId(diagnosisActivity.getPlanId());
        remActivity.setMeasurementId(diagnosisActivity.getMeasurementId());
        remActivity.setMeasurementTime(diagnosisActivity.getMeasurementTime());
        remActivity.setEventId(diagnosisActivity.getEventId());
        remActivity.setValue(diagnosisActivity.getValue());
        remActivity.setAffectedSlos(diagnosisActivity.getAffectedSlos());
        remActivity.setClassification(convertClassification(diagnosisActivity.getClassification()));
        remActivity.setEventImpact(diagnosisActivity.getEventImpact());
        remActivity.setRootCauses(diagnosisActivity.getRootCauses());
        remActivity.setPriorityQueueTimestamp(new Date());
        return remActivity;
    }

    private RemActivity.Classification convertClassification(DiagnosisActivity.Classification classification) throws DiagnosisException {
        switch (classification) {
            case ALERT:
                return RemActivity.Classification.ALERT;
            case VIOLATION:
                return RemActivity.Classification.VIOLATION;
            default:
                throw new DiagnosisException("Invalid DiagnosisActivity.Classification: " + classification);
        }
    }

    private void createSLAViolationAnnotation(Notification notification, DiagnosisActivity diagnosisActivity, List<Slo> affectedSlos,
                                              Measurement measurement, Map<String, Slo> metricSloMapping) throws DiagnosisException {
        List<String> affectedMetrics = new ArrayList<>();
        for (Slo slo : affectedSlos) {
            affectedMetrics.add(slo.getMetricId());
        }
        String affectedMetricsString = String.join(", ", affectedMetrics);

        Measurement.MonitoringEvent.Condition condition = measurement.getMonitoringEvent().getCondition();
        String thresholdValue;
        if (condition.getThreshold().startsWith("metric:")) {
            String metric = condition.getThreshold().substring("metric:".length());
            thresholdValue = metricSloMapping.get(metric).getValue();
        } else {
            thresholdValue = condition.getThreshold();
        }
        String conditionOperator = measurement.getMonitoringEvent().getCondition().getOperator();

        String rootCause = diagnosisActivity.getRootCauses().isEmpty() ? "N/A" :
                diagnosisActivity.getRootCauses().get(0);
        if (!rootCause.endsWith(".")) {
            rootCause += ".";
        }
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        String timestampString = df.format(notification.getTimestamp());

        String description;
        if (diagnosisActivity.getClassification() == Classification.ALERT) {
            description = String.format("SLA alert detected: caused by measurement %s with value '%s' at %s. " +
                            "Alert condition: %s %s. " +
                            "Affected SLOs: %s. Root cause: %s Event ID: %s.",
                    measurement.getId(), notification.getValue(), timestampString,
                    conditionOperator, thresholdValue,
                    affectedMetricsString, rootCause, diagnosisActivity.getId());
        } else if (diagnosisActivity.getClassification() == Classification.VIOLATION) {
            description = String.format("SLA violation detected: caused by measurement %s with value '%s' at %s. " +
                            "Violation condition: %s %s. " +
                            "Affected SLOs: %s. Root cause: %s Event ID: %s.",
                    measurement.getId(), notification.getValue(), timestampString,
                    conditionOperator, thresholdValue,
                    affectedMetricsString, rootCause, diagnosisActivity.getId());
        } else {
            throw new DiagnosisException("Unexpected classification: " + diagnosisActivity.getClassification());
        }

        slaManagerClient.addAnnotation(diagnosisActivity.getSlaId(), description);
    }

    private static Map<RiskLevelTableKey, Integer> createRiskLevelTable() {
        Map<RiskLevelTableKey, Integer> riskLevelTable = new HashMap<>();
        riskLevelTable.put(new RiskLevelTableKey(EventType.ALERT, Slo.ImportanceLevel.LOW), 1);
        riskLevelTable.put(new RiskLevelTableKey(EventType.ALERT, Slo.ImportanceLevel.MEDIUM), 2);
        riskLevelTable.put(new RiskLevelTableKey(EventType.ALERT, Slo.ImportanceLevel.HIGH), 3);
        riskLevelTable.put(new RiskLevelTableKey(EventType.VIOLATION, Slo.ImportanceLevel.LOW), 4);
        riskLevelTable.put(new RiskLevelTableKey(EventType.VIOLATION, Slo.ImportanceLevel.MEDIUM), 5);
        riskLevelTable.put(new RiskLevelTableKey(EventType.VIOLATION, Slo.ImportanceLevel.HIGH), 6);

        return riskLevelTable;
    }

    private static class RiskLevelTableKey {
        private EventType eventType;
        private Slo.ImportanceLevel importanceLevel;

        public RiskLevelTableKey(EventType eventType, Slo.ImportanceLevel importanceLevel) {
            this.eventType = eventType;
            this.importanceLevel = importanceLevel;
        }

        @Override
        public boolean equals(Object o) {
            RiskLevelTableKey that = (RiskLevelTableKey) o;
            return eventType == that.eventType && importanceLevel == that.importanceLevel;
        }

        @Override
        public int hashCode() {
            int result = eventType.hashCode();
            result = 31 * result + importanceLevel.hashCode();
            return result;
        }
    }

    private class RemediationEngine implements Runnable {
        private final Logger logger = LogManager.getLogger(RemediationEngine.class);
        private PriorityBlockingQueue<DiagnosisActivity> priorityQueue;

        public RemediationEngine() {
            Comparator<DiagnosisActivity> comparator = new Comparator<DiagnosisActivity>() {
                @Override
                public int compare(DiagnosisActivity diagnosisActivity1, DiagnosisActivity diagnosisActivity2) {
                    if (diagnosisActivity1.getEventImpact() == diagnosisActivity2.getEventImpact()) {
                        // older monitoring event has higher priority
                        return (int) (diagnosisActivity2.getCreationTime().getTime() - diagnosisActivity1.getCreationTime().getTime());
                    } else {
                        // monitoring event with higher event impact has higher priority
                        return diagnosisActivity1.getEventImpact() - diagnosisActivity2.getEventImpact();
                    }
                }
            };
            priorityQueue = new PriorityBlockingQueue<>(QUEUE_CAPACITY, comparator);
            logger.debug("RemediationEngine initialized successfully.");
        }

        public void add(DiagnosisActivity diagnosisActivity) throws DiagnosisException {
            try {
                priorityQueue.add(diagnosisActivity);
                logger.debug("Diagnosis activity {} has been added to the priority queue. Queue size: {}",
                        diagnosisActivity.getId(), priorityQueue.size());
            } catch (IllegalStateException e) {
                throw new DiagnosisException("Priority queue is full.", e);
            }
        }

        @Override
        public void run() {
            logger.debug("RemediationEngine started.");
            try {
                while (!Thread.currentThread().isInterrupted()) {
                    logger.debug("Waiting for events...");
                    DiagnosisActivity diagnosisActivity = priorityQueue.take();
                    logger.debug("Received new event from the priority queue: {}.", diagnosisActivity.getId());
                    processDiagnosisActivity(diagnosisActivity);
                }
            } catch (InterruptedException e) {
                logger.debug("RemediationEngine was interrupted.");
            }
        }

        private void processDiagnosisActivity(DiagnosisActivity diagnosisActivity) {
            try {
                diagnosisActivity.setState(DiagnosisActivity.Status.REMEDIATING);
                diagActRepository.save(diagnosisActivity);
                // TODO: get latest measurement from the monitoring-event-archiver
                RemActivity remActivityData = createRemActivityData(diagnosisActivity);
                if (logger.isTraceEnabled()) {
                    logger.trace("Remediation activity data:\n{}", JsonDumper.dump(remActivityData));
                }
                RemActivity remActivity = rdsClient.createRemActivity(remActivityData);

                // wait until the remediation activity finishes
                logger.debug("Waiting for the remediation activity {} to finish...", remActivity.getId());
                Date startTime = new Date();
                long timeout = (long) appConfig.getRemActivityTimeout() * 1000;
                RemActivity.Status status;
                do {
                    Thread.sleep(5000);
                    status = rdsClient.retrieveRemActivityStatus(remActivity.getId());

                    if (new Date().getTime() - startTime.getTime() > timeout) {
                        throw new DiagnosisException("Timeout waiting for the remediation activity to finish.");
                    }
                } while (status != RemActivity.Status.SOLVED && status != RemActivity.Status.ERROR);

                logger.debug("Remediation activity finished in {} seconds with status {}.",
                        (System.currentTimeMillis() - startTime.getTime()) / 1000.0, status);

                if (status == RemActivity.Status.SOLVED) {
                    logger.debug("Remediation activity {} finished successfully.", diagnosisActivity.getId());
                    diagnosisActivity.setState(DiagnosisActivity.Status.SOLVED);
                    diagActRepository.save(diagnosisActivity);
                    auditClient.logComponentActivity("Diagnosis", CompActivity.ComponentState.DEACTIVATED,
                            diagnosisActivity.getSlaId(), SlaState.OBSERVED);

                    String description;
                    if (diagnosisActivity.getClassification() == Classification.ALERT) {
                        description = String.format(
                                "Alert event %s resolved successfully.", diagnosisActivity.getId());
                    } else if (diagnosisActivity.getClassification() == Classification.VIOLATION) {
                        description = String.format(
                                "Violation event %s resolved successfully.", diagnosisActivity.getId());
                    } else {
                        throw new UnsupportedOperationException();
                    }
                    slaManagerClient.addAnnotation(diagnosisActivity.getSlaId(), description);

                } else if (status == RemActivity.Status.ERROR) {
                    logger.error("Remediation activity {} failed.", remActivity.getId());
                    remActivity = rdsClient.retrieveRemActivity(remActivity.getId());
                    String errorMsg = "N/A";
                    for (Annotation annotation : remActivity.getAnnotations()) {
                        if (Objects.equals(annotation.getName(), "error")) {
                            errorMsg = annotation.getValue();
                            break;
                        }
                    }

                    auditClient.logComponentActivity("Diagnosis", CompActivity.ComponentState.DEACTIVATED,
                            diagnosisActivity.getSlaId(), SlaState.OBSERVED);

                    String description = String.format("Remediation for the %s event %s failed. Error description: %s",
                            diagnosisActivity.getClassification().name().toLowerCase(),
                            diagnosisActivity.getId(), errorMsg);
                    slaManagerClient.addAnnotation(diagnosisActivity.getSlaId(), description);

                    throw new DiagnosisException(String.format("Remediation activity %s failed: %s",
                            remActivity.getId(), errorMsg));
                }

            } catch (Exception e) {
                logger.error("Diagnosis activity failed: " + e.getMessage(), e);
                diagnosisActivity.setState(DiagnosisActivity.Status.ERROR);
                diagnosisActivity.addAnnotation(
                        new Annotation("error", "Diagnosis activity failed: " + e.getMessage(), new Date()));
                diagActRepository.save(diagnosisActivity);
            }
        }
    }

    private enum DataType {
        INTEGER,
        DOUBLE,
        BOOLEAN
    }
}
