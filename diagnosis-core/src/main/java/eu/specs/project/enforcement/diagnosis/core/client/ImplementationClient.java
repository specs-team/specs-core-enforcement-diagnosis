package eu.specs.project.enforcement.diagnosis.core.client;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.specs.datamodel.enforcement.ImplementationPlan;
import eu.specs.project.enforcement.diagnosis.core.exception.DiagnosisException;
import eu.specs.project.enforcement.diagnosis.core.util.AppConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

@Component
public class ImplementationClient {
    private static String IMPL_PLAN_PATH = "/impl-plans/{id}";
    private static final Logger logger = LogManager.getLogger(ImplementationClient.class);

    private WebTarget implementationTarget;

    @Autowired
    public ImplementationClient(AppConfig appConfig) {
        Client client = ClientBuilder.newBuilder()
                .register(JacksonJsonProvider.class)
                .build();

        implementationTarget = client.target(appConfig.getImplementationApiAddress());
    }

    public ImplementationPlan retrieveImplPlan(String implPlanId) throws DiagnosisException {
        try {
            logger.debug("Retrieving implementation plan {}...", implPlanId);
            ImplementationPlan implPlan = implementationTarget
                    .path(IMPL_PLAN_PATH)
                    .resolveTemplate("id", implPlanId)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .get(ImplementationPlan.class);

            logger.debug("Implementation plan retrieved successfully.");
            return implPlan;
        } catch (Exception e) {
            throw new DiagnosisException("Failed to retrieve implementation plan: " + e.getMessage(), e);
        }
    }
}
