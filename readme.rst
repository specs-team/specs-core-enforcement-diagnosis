.. contents:: Table of Contents

========
Overview
========

The SPECS Core Enforcement Diagnosis component classifies, analyses, and prioritizes detected monitoring events.

During the SLA implementation phase, not only security mechanisms that enforce negotiated metrics are deployed but also components that are able to monitor them. Installed monitoring adapters continuously report about the status of the acquired resources and services running on top of them. Events are sent to the Monitoring module which aggregates, archives, and filters them according to the MoniPoli. Events that break at least one of the Monipoli rules are notified to the Diagnosis component.

The SLA remediation phase and the diagnosis process is presented with sequence diagrams in the figures below.

**SLA remediation phase:**

.. image:: https://bitbucket.org/specs-team/specs-core-enforcement-diagnosis/raw/master/docs/images/rem_phase.png

**Diagnosis process:**

.. image:: https://bitbucket.org/specs-team/specs-core-enforcement-diagnosis/raw/master/docs/images/diagnosis.png

More details about the component can be found in the deliverable 4.2.2 available  at `http://www.specs-project.eu/publications/public-deliverables/d4-2-2 <http://www.specs-project.eu/publications/public-deliverables/d4-2-2>`_.

============
Installation
============

Prerequisites:

- Java web container
- MongoDB
- Java 7

The Diagnosis component is packaged as a web application archive (WAR) file with the name diagnosis-api.war. The archive file can be built from source code or downloaded from the SPECS maven repository::

 https://nexus.services.ieat.ro/nexus/content/repositories/specs-snapshots/eu/specs-project/core/enforcement/diagnosis-api/

To build the Diagnosis component from source code you need the Apache Maven 3 tool. First clone the project from the Bitbucket repository using a Git client::

 git clone git@bitbucket.org:specs-team/specs-core-enforcement-diagnosis.git

then go into the specs-core-enforcement-diagnosis and run::

 mvn package

The diagnosis-api.war file is located in the diagnosis-api/target directory.

The diagnosis-api.war file has to be deployed to a Java web container. For example, to deploy the application to Apache Tomcat 7, just copy the war file to the Tomcat webapps directory::

 cp diagnosis-api/target/diagnosis-api.war /var/lib/tomcat7/webapps/

The application configuration is located in the file *diagnosis.properties* in the Java properties format. The file contains the following configuration properties:

.. code-block:: jproperties

 implementation-api.address=http://localhost:8080/implementation-api
 rds-api.address=http://localhost:8080/rds-api
 auditing-api.address=http://localhost:8080/sla-auditing
 sla-manager-api.address=http://localhost:8080/sla-manager/cloud-sla
 event-archiver-api.address=http://localhost:10101/

Make the necessary changes and restart the web container for changes to take effect. The Diagnosis API should now be available at::

 https://<host>:<port>/diagnosis-api

======
Notice
======

This product includes software developed at "XLAB d.o.o, Slovenia", as part of the "SPECS - Secure Provisioning of Cloud Services based on SLA Management" research project (an EC FP7-ICT Grant, agreement 610795).

- http://www.specs-project.eu/
- http://www.xlab.si/

Developers:

- Jolanda Modic, jolanda.modic@xlab.si
- Damjan Murn, damjan.murn@xlab.si
- Aljaž Košir, aljaz.kosir@xlab.si

Copyright:

.. code-block:: 

 Copyright 2013-2015, XLAB d.o.o, Slovenia
    http://www.xlab.si

 SPECS Core Enforcement Diagnosis is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License, version 3,
 as published by the Free Software Foundation.

 SPECS Core Enforcement Diagnosis is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.