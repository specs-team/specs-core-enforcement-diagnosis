package eu.specs.project.enforcement.diagnosis_api.controller;

import eu.specs.datamodel.common.ResourceCollection;
import eu.specs.datamodel.enforcement.DiagnosisActivity;
import eu.specs.datamodel.enforcement.Notification;
import eu.specs.project.enforcement.diagnosis.core.exception.DiagnosisException;
import eu.specs.project.enforcement.diagnosis.core.service.DiagnosisActivityService;
import eu.specs.project.enforcement.diagnosis_api.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@Controller
@RequestMapping(value = "/diag-activities")
public class DiagnosisActivityController {
    private static final String DIAG_ACT_PATH = "diag-activities/{id}";

    @Autowired
    private DiagnosisActivityService diagActService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    public DiagnosisActivity create(@RequestBody Notification notification) throws DiagnosisException {
        return diagActService.create(notification);
    }

    @RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResourceCollection getAllDiagActivities(
            @RequestParam(value = "slaId", required = false) String slaId,
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "limit", required = false) Integer limit) {
        DiagnosisActivityService.DiagActFilter filter = new DiagnosisActivityService.DiagActFilter();
        filter.setSlaId(slaId);
        filter.setOffset(offset);
        filter.setLimit(limit);
        List<DiagnosisActivity> diagnosisActivities = diagActService.findAll(filter);
        ResourceCollection collection = new ResourceCollection();
        for (DiagnosisActivity diagActivity : diagnosisActivities) {
            URI uri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .pathSegment(DIAG_ACT_PATH)
                    .buildAndExpand(diagActivity.getId())
                    .toUri();
            collection.addItem(new ResourceCollection.Item(diagActivity.getId(), uri.toString()));
        }
        collection.setResource("diag-activities");
        collection.setTotal(diagnosisActivities.size());
        return collection;
    }

    @RequestMapping(value = "/{diagActId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public DiagnosisActivity getDiagActivity(@PathVariable("diagActId") String diagActId) throws ResourceNotFoundException {
        DiagnosisActivity diagnosisActivity = diagActService.findById(diagActId);
        if (diagnosisActivity == null) {
            throw new ResourceNotFoundException(String.format("Diagnosis activity %s cannot be found.", diagActId));
        } else {
            return diagnosisActivity;
        }
    }

    @RequestMapping(value = "/{diagActId}/status", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public String getDiagActivityStatus(@PathVariable("diagActId") String diagActId) throws ResourceNotFoundException {
        DiagnosisActivity diagnosisActivity = diagActService.findById(diagActId);
        if (diagnosisActivity == null) {
            throw new ResourceNotFoundException(String.format("Diagnosis activity %s cannot be found.", diagActId));
        } else {
            return diagnosisActivity.getState().name();
        }
    }
}
