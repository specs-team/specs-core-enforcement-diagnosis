package eu.specs.project.enforcement.diagnosis_api.controller;

import eu.specs.datamodel.common.ResourceCollection;
import eu.specs.datamodel.enforcement.DiagnosisActivity;
import eu.specs.datamodel.enforcement.Notification;
import eu.specs.project.enforcement.diagnosis.core.exception.DiagnosisException;
import eu.specs.project.enforcement.diagnosis.core.service.DiagnosisActivityService;
import eu.specs.project.enforcement.diagnosis.core.service.NotificationService;
import eu.specs.project.enforcement.diagnosis_api.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value = "/notifications")
public class NotificationController {
    private static final String NOTIFICATION_PATH = "/notifications/{id}";

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private DiagnosisActivityService diagActService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    public DiagnosisActivity create(@RequestBody Notification notification) throws DiagnosisException {
        return diagActService.create(notification);
    }

    @RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResourceCollection getAllNotifications(
            @RequestParam(value = "slaId", required = false) String slaId,
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "component", required = false) String component,
            @RequestParam(value = "object", required = false) String object) {
        NotificationService.NotificationsFilter filter = new NotificationService.NotificationsFilter();
        filter.setSlaId(slaId);
        filter.setOffset(offset);
        filter.setLimit(limit);
        filter.setComponent(component);
        filter.setObject(object);
        List<Notification> notifications = notificationService.findAll(filter);
        ResourceCollection collection = new ResourceCollection();
        for (Notification notification : notifications) {
            URI uri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .pathSegment(NOTIFICATION_PATH)
                    .buildAndExpand(notification.getId())
                    .toUri();
            collection.addItem(new ResourceCollection.Item(notification.getId(), uri.toString()));
        }
        collection.setTotal(notifications.size());
        collection.setMembers(notifications.size());
        return collection;
    }

    @RequestMapping(value = "/{notificationId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Notification getNotification(@PathVariable("notificationId") String notificationId) throws ResourceNotFoundException {
        Notification notification = notificationService.findById(notificationId);
        if (notification == null) {
            throw new ResourceNotFoundException(String.format("Notification %s cannot be found.", notificationId));
        } else {
            return notification;
        }
    }
}
