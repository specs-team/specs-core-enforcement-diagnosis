package eu.specs.project.enforcement.diagnosis_api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fakemongo.Fongo;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.mongodb.DB;
import eu.specs.datamodel.common.ResourceCollection;
import eu.specs.datamodel.enforcement.DiagnosisActivity;
import eu.specs.datamodel.enforcement.Notification;
import eu.specs.project.enforcement.diagnosis.core.service.DiagnosisActivityService;
import eu.specs.project.enforcement.diagnosis.core.util.JsonDumper;
import eu.specs.project.enforcement.diagnosis_api.mock.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.SocketUtils;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:diagnosis-api-context-test.xml")
public class DiagnosisApiTest {
    private static final Logger logger = LogManager.getLogger(DiagnosisApiTest.class);

    @Autowired
    private EventArchiverMock eventArchiverMock;

    @Autowired
    private PlanningMock planningMock;

    @Autowired
    private ImplementationMock implementationMock;

    @Autowired
    private SLAManagerMock slaManagerMock;

    @Autowired
    private RdsMock rdsMock;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private WebApplicationContext wac;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(Integer.parseInt(System.getProperty("wiremock.port")));

    private MockMvc mockMvc;

    @Autowired
    public Fongo fongo;

    @BeforeClass
    public static void setSystemProps() {
        int port = SocketUtils.findAvailableTcpPort();
        logger.debug("Using port {} for WireMock server.", port);
        System.setProperty("wiremock.port", Integer.toString(port));
    }

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(this.wac).build();
    }

    @After
    public void tearDown() throws Exception {
        for (DB db : fongo.getUsedDatabases()) {
            logger.trace("Dropping database " + db.getName());
            db.dropDatabase();
        }
    }

    @Test
    public void fullTest() throws Exception {
        eventArchiverMock.init();
        planningMock.init();
        implementationMock.init();
        slaManagerMock.init();
        rdsMock.init();

        Notification notification = objectMapper.readValue(
                this.getClass().getResourceAsStream("/notification.json"), Notification.class);
        String slaId = notification.getSlaId();

        // send notification to Diagnosis (DiagnosisActivity is created)
        MvcResult result = mockMvc.perform(post("/notifications")
                .content(objectMapper.writeValueAsString(notification)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        DiagnosisActivity diagnosisActivity = objectMapper.readValue(
                result.getResponse().getContentAsString(), DiagnosisActivity.class);

        assertNotNull(diagnosisActivity.getId());
        assertEquals(diagnosisActivity.getState(), DiagnosisActivity.Status.RECEIVED);
        assertEquals(diagnosisActivity.getSlaId(), slaId);

        // wait until the diagnosis activity finishes
        Date startTime = new Date();
        DiagnosisActivity.Status status;
        do {
            logger.trace("Waiting for the diagnosis activity to finish...");
            Thread.sleep(1000);
            result = mockMvc.perform(get("/diag-activities/{0}/status", diagnosisActivity.getId()))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andReturn();
            status = DiagnosisActivity.Status.valueOf(
                    result.getResponse().getContentAsString());

            if (status == DiagnosisActivity.Status.ERROR) {
                fail("Diagnosis activity failed.");
            }
            if (new Date().getTime() - startTime.getTime() > 15000) {
                fail("Timeout waiting for the diagnosis activity to finish.");
            }
        } while (status != DiagnosisActivity.Status.SOLVED);

        // retrieve diagnosis activity
        result = mockMvc.perform(get("/diag-activities/{0}", diagnosisActivity.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        logger.trace("Diagnosis activity:\n{}", JsonDumper.dump(diagnosisActivity));

        DiagnosisActivity diagnosisActivity1 = objectMapper.readValue(
                result.getResponse().getContentAsString(), DiagnosisActivity.class);
        assertEquals(diagnosisActivity1.getEventImpact(), 3);
        assertEquals(diagnosisActivity1.getClassification(), DiagnosisActivity.Classification.ALERT);

        mockMvc.perform(get("/diag-activities")
                .param("slaId", slaId)
                .param("offset", "0")
                .param("limit", "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.resource", is("diag-activities")))
                .andExpect(jsonPath("$.total", is(1)));

        mockMvc.perform(get("/notifications/{0}", diagnosisActivity.getNotificationId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.notification_id", is(diagnosisActivity.getNotificationId())));

        // retrieve notification
        mockMvc.perform(get("/notifications/{0}", diagnosisActivity.getNotificationId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.notification_id", is(diagnosisActivity.getNotificationId())));

        // retrieve all notifications
        result = mockMvc.perform(get("/notifications")
                .param("slaId", slaId)
                .param("offset", "0")
                .param("limit", "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();
        ResourceCollection collection = objectMapper.readValue(
                result.getResponse().getContentAsString(), ResourceCollection.class);
        assertEquals(collection.getTotal(), 1);
        assertEquals(collection.getItemList().get(0).getId(), diagnosisActivity.getNotificationId());
    }

    @Test
    public void testErrorResponse() throws Exception {
        mockMvc.perform(get("/diag-activities/{0}", "invalid"))
                .andExpect(status().isNotFound());

        mockMvc.perform(get("/diag-activities/{0}/status", "invalid"))
                .andExpect(status().isNotFound());

        mockMvc.perform(get("/notifications/{0}", "invalid"))
                .andExpect(status().isNotFound());
    }
}